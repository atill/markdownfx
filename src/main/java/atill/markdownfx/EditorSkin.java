/**
 * Copyright 2013 Andy Till
 * 
 * This file is part of markdownfx.
 * 
 * EstiMate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * EstiMate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with markdownfx.  If not, see <http://www.gnu.org/licenses/>.
 */

package atill.markdownfx;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;

import com.sun.javafx.scene.control.skin.TextAreaSkin;

/**
 * The {@link EditorSkin} is a work-around to allow additional items to be added
 * to the context menu of the text area. At the time of writing, this is not
 * available without sub-classing the skin.
 * 
 * @author atill
 * 
 */
public class EditorSkin extends TextAreaSkin {
    
    private final  ActionBuilder actionBuilder;

    public EditorSkin(TextArea textArea) {
        super(textArea);
        
        actionBuilder = new ActionBuilder(new TextSourceAdapter(textArea));
    }
    
    @Override
    public void populateContextMenu(ContextMenu menu) {
        super.populateContextMenu(menu);

        Menu markdownMenu;
        
        markdownMenu = new Menu("Markdown");
        markdownMenu.getItems().add(menuItem("Header 1", actionBuilder.insertH1()));
        markdownMenu.getItems().add(menuItem("Header 2", actionBuilder.insertH2()));
        markdownMenu.getItems().add(menuItem("Unordered List", actionBuilder.insertUL()));
        markdownMenu.getItems().add(menuItem("Link", actionBuilder.insertLink()));
        
        menu.getItems().add(0, markdownMenu);
    }
    
    public MenuItem menuItem(String text, EventHandler<ActionEvent> actionHandler) {
        MenuItem menuItem;
        
        menuItem = new MenuItem(text);
        menuItem.setOnAction(actionHandler);

        return menuItem;
    }
}
