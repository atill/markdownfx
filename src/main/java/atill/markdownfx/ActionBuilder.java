/**
 * Copyright 2013 Andy Till
 * 
 * This file is part of markdownfx.
 * 
 * EstiMate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * EstiMate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with markdownfx.  If not, see <http://www.gnu.org/licenses/>.
 */

package atill.markdownfx;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

class ActionBuilder {

    private static final String H1_MARKDOWN = "\n=========\n";

    private static final String H2_MARKDOWN = "\n---------\n";

    private static final String H1 = "The Title" + H1_MARKDOWN;

    private static final String H2 = "The Title" + H2_MARKDOWN;
    
    private final TextSourceAdapter textSourceAdapter;

    public ActionBuilder(TextSourceAdapter aTextSourceAdapter) {
        textSourceAdapter = aTextSourceAdapter;
    }
        
    public EventHandler<ActionEvent> insert(String insert, String insertWhenSelected) {
        return new InsertTextAction(insert, insertWhenSelected, textSourceAdapter);
    }

    public EventHandler<ActionEvent> insertLink() {
        return new InsertTextAction("[example link](http://example.com/)", "[", "](http://example.com/)", textSourceAdapter);
    }

    public EventHandler<ActionEvent> insertUL() {
        return new UnorderedListAction(textSourceAdapter);
    }

    public EventHandler<ActionEvent> insertH1() {
        return insert(H1, H1_MARKDOWN);
    }
    
    public EventHandler<ActionEvent> insertH2() {
        return insert(H2, H2_MARKDOWN);
    }
}
