/**
 * Copyright 2013 Andy Till
 * 
 * This file is part of markdownfx.
 * 
 * EstiMate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * EstiMate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with markdownfx.  If not, see <http://www.gnu.org/licenses/>.
 */

package atill.markdownfx;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

class UnorderedListAction implements EventHandler<ActionEvent> {
    
    private static final String UL_MARKDOWN = " + ";
    
    private final TextSourceAdapter textSourceAdapter;

    public UnorderedListAction(TextSourceAdapter aTextSourceAdapter) {
        textSourceAdapter = aTextSourceAdapter;
    }
    
    public void handle(ActionEvent e) {
        if(textSourceAdapter.isTextSelected()) {
            String text = textSourceAdapter.getSelectedText();
            String[] splitText = text.split("\n");
            
            String newText = "";
            for (int i = 0; i < splitText.length; i++) {
                newText += ("\n" + UL_MARKDOWN + splitText[i]);
            }
            
            textSourceAdapter.replaceSelection(newText);
        }
        else {
            textSourceAdapter.insert(UL_MARKDOWN);
        }
    }
}
