/**
 * Copyright 2013 Andy Till
 * 
 * This file is part of markdownfx.
 * 
 * EstiMate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * EstiMate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with markdownfx.  If not, see <http://www.gnu.org/licenses/>.
 */

package atill.markdownfx;

import java.net.URL;
import java.util.ResourceBundle;

import org.pegdown.PegDownProcessor;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.web.WebView;

public class EditorAppController implements Initializable {
    @FXML
    private EditorController mdEditorController;
    @FXML
    private WebView mdViewer;

    public void initialize(URL url, ResourceBundle resources) {
        // set a nicer stylesheet for the webview than what is default, this is taken from
        // markdowncss project, https://bitbucket.org/kevinburke/markdowncss
        mdViewer.getEngine().setUserStyleSheetLocation(getClass().getResource("/atill/markdownfx/markdown.css").toExternalForm());
        
        final PegDownProcessor processor = new PegDownProcessor();
        
        mdEditorController.markdownProperty().addListener(new ChangeListener<String>() {
            public void changed(ObservableValue<? extends String> arg0, String old, String newString) {
                String html = processor.markdownToHtml(newString);
                mdViewer.getEngine().loadContent(html);
            }});
    }
}
