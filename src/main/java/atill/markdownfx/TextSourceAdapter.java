/**
 * Copyright 2013 Andy Till
 * 
 * This file is part of markdownfx.
 * 
 * EstiMate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * EstiMate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with markdownfx.  If not, see <http://www.gnu.org/licenses/>.
 */

package atill.markdownfx;

import javafx.scene.control.TextArea;

/**
 * {@link TextSourceAdapter} provides a simple API for working with text in the
 * text input control.
 * 
 * @author atill
 * 
 */
class TextSourceAdapter {
    private final TextArea textArea;

    public TextSourceAdapter(TextArea aTextArea) {
        this.textArea = aTextArea;
    }
    
    public void insert(String text) {
        textArea.insertText(textArea.getCaretPosition(), text);
    }

    public void insertBeforeSelection(String text) {
        textArea.insertText(textArea.getSelection().getStart(), text);
    }

    public void insertAfterSelection(String text) {
        textArea.insertText(textArea.getSelection().getEnd(), text);
    }

    public boolean isTextSelected() {
        return textArea.getSelectedText() != null && !textArea.getSelectedText().isEmpty();
    }

    public String getSelectedText() {
        return textArea.getSelectedText();
    }

    public void replaceSelection(String newText) {
        textArea.replaceSelection(newText);
    }

    public void insertBeforeAndAfterSelection(String beforeSelectionText, String afterSelectionText) {
        int start = textArea.getSelection().getStart();
        int end = textArea.getSelection().getEnd();

        textArea.insertText(start, beforeSelectionText);
        textArea.insertText(end + beforeSelectionText.length(), afterSelectionText);
    }
}
