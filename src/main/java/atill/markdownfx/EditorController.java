/**
 * Copyright 2013 Andy Till
 * 
 * This file is part of markdownfx.
 * 
 * EstiMate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * EstiMate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with markdownfx.  If not, see <http://www.gnu.org/licenses/>.
 */

package atill.markdownfx;

import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

import javafx.beans.property.ReadOnlyStringProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyEvent;


public class EditorController implements Initializable {
    @FXML
    private TextArea editor;
    @FXML
    private Button h1Button;
    @FXML
    private Button h2Button;
    @FXML
    private Button ulButton;
    @FXML
    private Button linkButton;

    public void initialize(URL url, ResourceBundle resources) {
        ActionBuilder actionBuilder = new ActionBuilder(new TextSourceAdapter(editor));
        
        final EventHandler<ActionEvent> h1, h2, ul, link;
        
        h1 = actionBuilder.insertH1();
        h2 = actionBuilder.insertH2();
        ul = actionBuilder.insertUL();
        link = actionBuilder.insertLink();
        
        h1Button.setOnAction(h1);
        h2Button.setOnAction(h2);
        ulButton.setOnAction(ul);
        linkButton.setOnAction(link);
        
        @SuppressWarnings("serial")
        final HashMap<String, EventHandler<ActionEvent>> keymap = new HashMap<String, EventHandler<ActionEvent>>() {{
            put("1", h1);
            put("2", h2);
            put("u", ul);
            put("l", link);
        }};
        
        editor.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent e) {
                if(e.isControlDown()) {
                    EventHandler<ActionEvent> handler = keymap.get(e.getText());
                    
                    if(handler != null) {
                        handler.handle(null);
                    }
                }
            }});
    }

    public ReadOnlyStringProperty markdownProperty() {
        return editor.textProperty();
    }
}
