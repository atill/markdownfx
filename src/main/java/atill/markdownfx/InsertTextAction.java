/**
 * Copyright 2013 Andy Till
 * 
 * This file is part of markdownfx.
 * 
 * EstiMate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * EstiMate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with markdownfx.  If not, see <http://www.gnu.org/licenses/>.
 */

package atill.markdownfx;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

class InsertTextAction implements EventHandler<ActionEvent> {
    
    private final String text;

    private final String beforeSelectionText;

    private final String afterSelectionText;
    
    private final TextSourceAdapter textSourceAdapter;

    public InsertTextAction(String theText, String afterSelectionText, TextSourceAdapter aTextSourceAdapter) {
        this(theText, null, afterSelectionText, aTextSourceAdapter);
    }
    
    public InsertTextAction(String theText, String beforeSelectionText, String afterSelectionText, TextSourceAdapter aTextSourceAdapter) {
        text = theText;
        this.beforeSelectionText = beforeSelectionText;
        this.afterSelectionText = afterSelectionText;
        textSourceAdapter = aTextSourceAdapter;
    }

    public void handle(ActionEvent e) {
        if(textSourceAdapter.isTextSelected()) {
            if(beforeSelectionText != null && afterSelectionText != null) {
                textSourceAdapter.insertBeforeAndAfterSelection(beforeSelectionText, afterSelectionText);
            }
            else {
                if(beforeSelectionText != null) {
                    textSourceAdapter.insertBeforeSelection(beforeSelectionText);
                }
                
                if(afterSelectionText != null) {
                    textSourceAdapter.insertAfterSelection(afterSelectionText);
                }
            }
        }
        else {
            textSourceAdapter.insert(text);
        }
    }
}
